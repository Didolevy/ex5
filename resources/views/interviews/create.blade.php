@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
<h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
    
        @csrf 
        <div class="form-group">
            <label for = "name">Interview summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div>     
                <input type = "submit" name = "submit" value = "Create candidate">
        </div>                       
        </form>    
@endsection
