@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

@if(Auth::user()->id == 1)
       
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
@endif
        

<h1>interview List</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>date</th><th>summary</th><th>candidate</th><th>user</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->summary}}</td>
           
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($interview->candidate_id))
                          {{$interview->candidate->name}}  
                        @else
                            None
                        @endif

                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                    <a class="dropdown-item" href="{{route('interviews.changecandi',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a>
                     
                    @endforeach
                    </div>
                  </div>                
            </td>
<td>
<div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($interview->user_id))
                          {{$interview->user_id}}  
                        @else
                            None
                        @endif

                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                    <a class="dropdown-item" href="{{route('interviews.changeUser',[$interview->id,$user->id])}}">{{$user->name}}</a>
                     
                    @endforeach
                    </div>
                  </div>                
            </td>
     
            </td>
            
    @endforeach
</table>
@endsection

