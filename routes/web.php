<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
|  Routes
|--------------------------------------------------------------------------
|
| Here is where you can register  routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('/hello',function(){
return'Hello Larevel';

//http://localhost/recruit/public/student/1
});
Route::get('/student/{id}',function($id='No student found'){
    return 'we got student with id'.$id;

});
Route::get('/car/{id?}',function($id = null){
    if(isset($id)){
        return "we got car $id";
    } else{
        return 'we need the id to find your car';
    }

});
//http://localhost/recruit/public/comment/1 with the comment page in view
Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

//class 6 of candidates

Route::resource('candidates', 'CandidatesController');
Route::resource('interviews', 'InterviewsController');
//Ex5
Route::get('/users/{email}/{name?}', function ($email,$name = null) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});


Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//class 9 of dropdown assign
Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changeStatus/{cid}/{sid}','CandidatesController@changeStatus')->name('candidate.changestatus');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidate.mycandidates')->middleware('auth');
Route::get('/interviews', 'InterviewsController@index')->name('interview.index')->middleware('auth');

Route::get('interviews/changecandi/{iid}/{cid?}', 'InterviewsController@changecandi')->name('interviews.changecandi');
Route::get('interviews/changeUser/{iid}/{uid?}', 'InterviewsController@changeUser')->name('interviews.changeUser');