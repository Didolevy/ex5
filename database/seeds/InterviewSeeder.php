<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'id' => 2,
                'summary'=>'good one',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                
            ],
            [
                'id' => 3,
                'summary'=>'not good',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);         
    }

    }

